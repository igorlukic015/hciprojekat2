﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace MapaZaduzbinara
{
    class Pomoc
    {
        public static readonly DependencyProperty HelpKeyProperty = DependencyProperty.RegisterAttached("HelpKey",
            typeof(string), typeof(Pomoc));

        public static string GetHelpKey(DependencyObject obj)
        {
            return obj.GetValue(HelpKeyProperty) as string;
        }

        public static void SetHelpKey(DependencyObject obj, string value)
        {
            obj.SetValue(HelpKeyProperty, value);
        }
        
        public static void PrikaziPomoc(string key, Window prikaz)
        {
            PomocPrikaz pomocPrikaz = new PomocPrikaz(key, prikaz);
            pomocPrikaz.Show();
        }
    }
}
