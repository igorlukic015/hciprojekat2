﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MapaZaduzbinara.Model;
using System.Text.RegularExpressions;
using System.Collections.ObjectModel;
using MapaZaduzbinara.Dialog;
using System.Data;

namespace MapaZaduzbinara
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : INotifyPropertyChanged
    {
        private string ime;
        private string prezime;
        private DateTime datumRodjenja;
        private DateTime datumSmrti;
        private string mesto;

        private string slika = null;
        private VrstaZaduzbine vrstaZaduzbine;

        private BazaPodataka baza;

        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string v)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(v));
            }
        }

        private ObservableCollection<Zaduzbina> zaduzbine = new ObservableCollection<Zaduzbina>();
        private ObservableCollection<Zaduzbinar> zaduzbinari = new ObservableCollection<Zaduzbinar>();
        private ObservableCollection<Vladar> vladari = new ObservableCollection<Vladar>();
        private ObservableCollection<Osoba> osobe = new ObservableCollection<Osoba>();

        public ObservableCollection<Zaduzbinar> Zaduzbinari
        {
            get { return zaduzbinari; }
            set { zaduzbinari = value; }
        }

        public ObservableCollection<Zaduzbina> Zaduzbine
        {
            get { return zaduzbine; }
            set { zaduzbine = value; }
        }
        public ObservableCollection<Osoba> Osobe
        {
            get { return osobe; }
            set { osobe = value; }
        }
        public ObservableCollection<Vladar> Vladari
        {
            get { return vladari; }
            set { vladari = value; }
        }

        public string Mesto
        {
            get
            {
                return mesto;
            }
            set
            {
                if (value != mesto)
                {
                    mesto = value;
                    OnPropertyChanged("Mesto");
                }
            }
        }

        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                if (value != ime)
                {
                    ime = value;
                    OnPropertyChanged("Ime");
                }
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }
            set
            {
                if (value != prezime)
                {
                    prezime = value;
                    OnPropertyChanged("Prezime");
                }
            }
        }

        public DateTime DatumRodjenja
        {
            get
            {
                return datumRodjenja;
            }
            set
            {
                if (value != datumRodjenja)
                {
                    datumRodjenja = value;
                    OnPropertyChanged("DatumRodjenja");
                }
            }
        }

        public DateTime DatumSmrti
        {
            get
            {
                return datumSmrti;
            }
            set
            {
                if (value != datumSmrti)
                {
                    datumSmrti = value;
                    OnPropertyChanged("DatumSmrti");
                }
            }
        }

        public MainWindow()
        {
            InitializeComponent();
            baza = new BazaPodataka();
        }

        private void TabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void TabControl_SelectionChanged_1(object sender, SelectionChangedEventArgs e)
        {

        }

        private void izaberiSlikuVladar_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Izaberi ikonicu";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                slikaVladar.Source = new BitmapImage(new Uri(fileDialog.FileName));
                slika = fileDialog.FileName;
            }
        }

        private void izaberiSlikuZaduzbina_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Izaberi ikonicu";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                slikaZaduzbina.Source = new BitmapImage(new Uri(fileDialog.FileName));
                slika = fileDialog.FileName;
            }
        }

        private void izaberiSlikuZaduzbinar_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Izaberi ikonicu";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                slikaZaduzbinar.Source = new BitmapImage(new Uri(fileDialog.FileName));
                slika = fileDialog.FileName;
            }
        }

        private void DodajVladara_Click(object sender, RoutedEventArgs e)
        {
            string ime = ime_textBoxVladar.Text;
            string prezime = prezime_textBoxVladar.Text;
            if (datumPickerRodjenjeVladar.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum rodjenja vladara");
                return;
            }

            if (datumPickerSmrtiVladar.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum smrti vladara");
                return;
            }

            DateTime datumRodjenja = (DateTime)datumPickerRodjenjeVladar.SelectedDate;
            DateTime datumSmrti = (DateTime)datumPickerSmrtiVladar.SelectedDate;
            string mesto = textBoxMestoRodjenjaVladar.Text;
            string dinastija = dinastija_textBoxVladar.Text;
            string titula = titula_textBoxVladar.Text;
            string drzava = drzava_textBoxVladar.Text;
            string prestonica = prestonica_textBoxVladar.Text;

            if (ime.Equals("") || prezime.Equals("") || datumRodjenja.Equals("") || datumSmrti.Equals("") || mesto.Equals("") || dinastija.Equals("") || titula.Equals("") || drzava.Equals("") || prestonica.Equals(""))
            {
                System.Windows.MessageBox.Show("Niste popunili sva polja za unos vladara");
                return;
            }

            if (!Regex.IsMatch(ime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Ime vladara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(prezime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Prezime vladara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(mesto, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Mesto rodjenja vladara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(titula, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Titula vladara ne sme da sadrzi brojeve");
                return;
            }
            if (!Regex.IsMatch(dinastija, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Dinastija vladara ne sme da sadrzi brojeve");
                return;
            }
            if (!Regex.IsMatch(drzava, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Drzava vladara ne sme da sadrzi brojeve");
                return;
            }
            if (!Regex.IsMatch(prestonica, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Prestonica vladara ne sme da sadrzi brojeve");
                return;
            }

            if (slika == null)
            {
                string privremeni = slikaVladar.Source.ToString();
                string[] privTokens = privremeni.Split(',');
                //slika = privTokens[3];
                slika = privremeni;

            }

            Vladar vladar = new Vladar(ime, prezime, datumRodjenja, datumSmrti, mesto, slika, dinastija, titula, drzava, prestonica);
            Zaduzbina z = new Zaduzbina(ime, prezime, 1000, "", vladar, VrstaZaduzbine.USTANOVA_KULTURE, slika);
            slika = null;
            slikaVladar.Source = new BitmapImage(new Uri("pack://application:,,,/MapaZaduzbinara;component/Icons/beer.png"));
            //System.Windows.MessageBox.Show(z.ToString());
            bool passed = baza.noviVladar(vladar);
            if (passed)
            {
                System.Windows.MessageBox.Show("Uspešno dodat novi vladar.", "Dodavanje vladara");
                baza.sacuvajVladare();
                vladari = baza.Vladari;
                osobe.Clear();
                foreach (Osoba o in zaduzbinari)
                {
                    osobe.Add(o);
                }
                foreach (Osoba o in vladari)
                {
                    osobe.Add(o);
                }
                zaduzbinar_comboBoxZaduzbina.ItemsSource = osobe;
            }
            else
                System.Windows.MessageBox.Show("Vec postoji uneti vladar u bazi podataka!", "Dodavanje vladara");
        }

        private void DodajZaduzbinara_Click(object sender, RoutedEventArgs e)
        {
            string ime = ime_textBoxZaduzbinar.Text;
            string prezime = prezime_textBoxZaduzbinar.Text;
            if (datumPickerRodjenjeZaduzbinar.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum rodjenja zaduzbinara");
                return;
            }

            if (datumPickerSmrtiZaduzbinar.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum smrti zaduzbinara");
                return;
            }
            DateTime datumRodjenja = (DateTime)datumPickerRodjenjeZaduzbinar.SelectedDate;
            DateTime datumSmrti = (DateTime)datumPickerSmrtiZaduzbinar.SelectedDate;
            string mesto = textBoxMestoRodjenjaZaduzbinar.Text;
            string zanimanje = zanimanje_textBoxZaduzbinar.Text;
            if (ime.Equals("") || prezime.Equals("") || datumRodjenja.Equals("") || datumSmrti.Equals("") || mesto.Equals("") || zanimanje.Equals(""))
            {
                System.Windows.MessageBox.Show("Niste popunili sva tražena polja za unos zadužbinara");
                return;
            }
            if (!Regex.IsMatch(ime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Ime zaduzbinara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(prezime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Prezime zaduzbinara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(mesto, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Mesto rodjenja zaduzbinara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(zanimanje, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Zanimanje zaduzbinara ne sme da sadrzi brojeve");
                return;
            }

            if (slika == null)
            {
                string privremeni = slikaZaduzbinar.Source.ToString();
                string[] privTokens = privremeni.Split(',');
                //slika = privTokens[3];
                slika = privremeni;

            }
            Zaduzbinar zaduzbinar = new Zaduzbinar(ime, prezime, datumRodjenja, datumSmrti, mesto, slika, zanimanje);
            slika = null;
            slikaZaduzbinar.Source = new BitmapImage(new Uri("pack://application:,,,/MapaZaduzbinara;component/Icons/beer.png"));
            //System.Windows.MessageBox.Show(zaduzbinar.ToString());
            bool passed = baza.noviZaduzbinar(zaduzbinar);
            if (passed)
            {
                System.Windows.MessageBox.Show("Uspešno dodat novi zadužbinar.", "Dodavanje zadužbinara");
                baza.sacuvajZaduzbinare();
                zaduzbinari = baza.Zaduzbinari;
                osobe.Clear();
                foreach (Osoba o in zaduzbinari)
                {
                    osobe.Add(o);
                }
                foreach (Osoba o in vladari)
                {
                    osobe.Add(o);
                }
                zaduzbinar_comboBoxZaduzbina.ItemsSource = osobe;

            }
            else
                System.Windows.MessageBox.Show("Vec postoji uneti zadužbinar u bazi podataka!", "Dodavanje zadužbinara");


        }


        private void DodajZaduzbina_Click(object sender, RoutedEventArgs e)
        {
            string naziv = naziv_textBoxZaduzbina.Text;
            string mesto = mesto_textBoxZaduzbina.Text;
            string godinaIzgradnje = godinaIzgradnje_textBoxZaduzbina.Text;
            string zaduzbinar = zaduzbinar_comboBoxZaduzbina.Text;
            string opis = textBoxOpisZaduzbina.Text;

            if (naziv.Equals("") || mesto.Equals("") || godinaIzgradnje.Equals("") || zaduzbinar.Equals(""))
            {
                System.Windows.MessageBox.Show("Niste popunili sva tražena polja za unos zadužbine");
                return;
            }

            if (!Regex.IsMatch(naziv, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Naziv zaduzbine ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(naziv, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Mesto zaduzbine ne sme da sadrzi brojeve");
                return;
            }

            int godina = -1;
            try
            {
                godina = Int32.Parse(godinaIzgradnje);
                Console.WriteLine(godina);
            }
            catch (FormatException)
            {
                Console.WriteLine($"Godina izgradnje mora biti ceo broj '{godinaIzgradnje}'");
                return;
            }

            if (slika == null)
            {
                string privremeni = slikaZaduzbina.Source.ToString();
                string[] privTokens = privremeni.Split(',');
                //slika = privTokens[3];
                slika = privremeni;

            }

            Osoba z = (Osoba)zaduzbinar_comboBoxZaduzbina.SelectedValue;
            Zaduzbina zaduzbina = new Zaduzbina(naziv, mesto, godina, opis, z, vrstaZaduzbine, slika);

            slika = null;
            slikaZaduzbina.Source = new BitmapImage(new Uri("pack://application:,,,/MapaZaduzbinara;component/Icons/beer.png"));
            //System.Windows.MessageBox.Show(zaduzbina.ToString());
            bool passed = baza.novaZaduzbina(zaduzbina);
            if (passed)
            {
                System.Windows.MessageBox.Show("Uspešno dodata nova zadužbina.", "Dodavanje zadužbina");
                baza.sacuvajZaduzbine();
                zaduzbine = baza.Zaduzbine;

            }
            else
                System.Windows.MessageBox.Show("Vec postoji uneta zadužbina u bazi podataka!", "Dodavanje zadužbina");

        }

        private void VerskiObjekat_Checked(object sender, RoutedEventArgs e)
        {
            vrstaZaduzbine = VrstaZaduzbine.VERSKI_OBJEKAT;
        }

        private void UstanovaKulture_Checked(object sender, RoutedEventArgs e)
        {
            vrstaZaduzbine = VrstaZaduzbine.USTANOVA_KULTURE;
        }

        private void PrikaziUstanoveKulture_Click(object sender, RoutedEventArgs e)
        {
            ObservableCollection<Zaduzbina> kulture = new ObservableCollection<Zaduzbina>();
            foreach (Zaduzbina z in zaduzbine)
            {
                if (z.Vrsta.Equals(VrstaZaduzbine.USTANOVA_KULTURE))
                {
                    kulture.Add(z);
                }
            }
            dataGridKulture.ItemsSource = kulture;
            dataGridVladar.Visibility = Visibility.Hidden;
            dataGridZaduzbinar.Visibility = Visibility.Hidden;
            dataGridKulture.Visibility = Visibility.Visible;
            dataGridVerski.Visibility = Visibility.Hidden;
        }

        private void PrikaziVerskiObjekat_Click(object sender, RoutedEventArgs e)
        {

            ObservableCollection<Zaduzbina> verski = new ObservableCollection<Zaduzbina>();
            foreach (Zaduzbina z in zaduzbine)
            {
                if (z.Vrsta.Equals(VrstaZaduzbine.VERSKI_OBJEKAT))
                {
                    verski.Add(z);
                }
            }
            dataGridVerski.ItemsSource = verski;
            dataGridVladar.Visibility = Visibility.Hidden;
            dataGridZaduzbinar.Visibility = Visibility.Hidden;
            dataGridKulture.Visibility = Visibility.Hidden;
            dataGridVerski.Visibility = Visibility.Visible;
        }

        private void PrikaziVladare_Click(object sender, RoutedEventArgs e)
        {
            dataGridVladar.ItemsSource = vladari;
            dataGridVladar.Visibility = Visibility.Visible;
            dataGridZaduzbinar.Visibility = Visibility.Hidden;
            dataGridKulture.Visibility = Visibility.Hidden;
            dataGridVerski.Visibility = Visibility.Hidden;

        }

        private void PrikaziZaduzbinare_Click(object sender, RoutedEventArgs e)
        {

            dataGridZaduzbinar.ItemsSource = zaduzbinari;
            dataGridVladar.Visibility = Visibility.Hidden;
            dataGridZaduzbinar.Visibility = Visibility.Visible;
            dataGridKulture.Visibility = Visibility.Hidden;
            dataGridVerski.Visibility = Visibility.Hidden;
        }

        private void IzmenaVladar(object sender, RoutedEventArgs e)
        {
            try
            {
                Vladar vladar = (Vladar)((Button)e.Source).DataContext;
                IzmenaVladara izmena = new IzmenaVladara(vladar, baza);
                izmena.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void IzmenaZaduzbinar(object sender, RoutedEventArgs e)
        {
            try
            {
                Zaduzbinar zaduzbinar = (Zaduzbinar)((Button)e.Source).DataContext;
                IzmenaZaduzbinara izmena = new IzmenaZaduzbinara(zaduzbinar, baza);
                izmena.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void IzmenaVerski(object sender, RoutedEventArgs e)
        {
            try
            {
                Zaduzbina zaduzbina = (Zaduzbina)((Button)e.Source).DataContext;
                IzmenaZaduzbina izmena = new IzmenaZaduzbina(zaduzbina, baza, osobe);
                izmena.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void CommandBinding_Executed(object sender, ExecutedRoutedEventArgs e)
        {
            IInputElement f = FocusManager.GetFocusedElement(Application.Current.Windows[0]);
            if (f is DependencyObject)
            {
                string key = Pomoc.GetHelpKey((DependencyObject)f);
                Pomoc.PrikaziPomoc(key, this);
            }
        }

        private void Pomoc_Click(object sender, RoutedEventArgs e)
        {
            PomocPrikaz pomocPrikaz = new PomocPrikaz("Pocetna", this);
            pomocPrikaz.Show();
        }

        private void IzmenaKulture(object sender, RoutedEventArgs e)
        {
            try
            {
                Zaduzbina zaduzbina = (Zaduzbina)((Button)e.Source).DataContext;
                IzmenaZaduzbina izmena = new IzmenaZaduzbina(zaduzbina, baza, osobe);
                izmena.Show();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }
        private void BrisanjeVladar(object sender, RoutedEventArgs e)
        {
            try
            {
                Vladar vladar = (Vladar)((Button)e.Source).DataContext;
                baza.brisanjeVladara(vladar);
                dataGridVladar.ItemsSource = vladari;
                dataGridVladar.Visibility = Visibility.Visible;
                dataGridZaduzbinar.Visibility = Visibility.Hidden;
                dataGridKulture.Visibility = Visibility.Hidden;
                dataGridVerski.Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }
        }




        private void BrisanjeZaduzbinar(object sender, RoutedEventArgs e)
        {
            try
            {
                Zaduzbinar zaduzbinar = (Zaduzbinar)((Button)e.Source).DataContext;
                baza.brisanjeZaduzbinara(zaduzbinar);
                dataGridZaduzbinar.ItemsSource = zaduzbinari;
                dataGridVladar.Visibility = Visibility.Hidden;
                dataGridZaduzbinar.Visibility = Visibility.Visible;
                dataGridKulture.Visibility = Visibility.Hidden;
                dataGridVerski.Visibility = Visibility.Hidden;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message.ToString());
            }


        }
        private void BrisanjeVerski(object sender, RoutedEventArgs e)
        {

        }
        private void BrisanjeKulture(object sender, RoutedEventArgs e)
        {

        }
    }

}


public enum VrstaZaduzbine
{
    VERSKI_OBJEKAT,
    USTANOVA_KULTURE
}

