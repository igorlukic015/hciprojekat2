﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapaZaduzbinara.Model
{
    public abstract class Osoba: INotifyPropertyChanged
    {
        private string ime;
        private string prezime;
        private DateTime datumRodjenja;
        private DateTime datumSmrti;
        private string mesto;
        private string slika;

        public string Ime
        {
            get
            {
                return ime;
            }
            set
            {
                if (value != ime)
                {
                    ime = value;
                    OnPropertyChanged("Ime");
                }
            }
        }

        public string Prezime
        {
            get
            {
                return prezime;
            }
            set
            {
                if (value != prezime)
                {
                    prezime = value;
                    OnPropertyChanged("Prezime");
                }
            }
        }

        public string Mesto
        {
            get
            {
                return mesto;
            }
            set
            {
                if (value != mesto)
                {
                    mesto = value;
                    OnPropertyChanged("Mesto");
                }
            }
        }
        public DateTime DatumRodjenja
        {
            get
            {
                return datumRodjenja;
            }
            set
            {
                if (value != datumRodjenja)
                {
                    datumRodjenja = value;
                    OnPropertyChanged("DatumRodjenja");
                }
            }
        }

        public DateTime DatumSmrti
        {
            get
            {
                return datumSmrti;
            }
            set
            {
                if (value != datumSmrti)
                {
                    datumSmrti = value;
                    OnPropertyChanged("DatumSmrti");
                }
            }
        }

        public string Slika
        {
            get
            {
                return slika;
            }
            set
            {
                if (value != slika)
                {
                    slika = value;
                    OnPropertyChanged("Slika");
                }
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        public virtual void OnPropertyChanged(string v)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(v));
            }
        }
    }

    
}
