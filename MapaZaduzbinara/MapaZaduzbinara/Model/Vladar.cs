﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapaZaduzbinara.Model
{
    public class Vladar: Osoba, INotifyPropertyChanged
    {
        private string dinastija;
        private string titula;
        private string drzava;
        private string prestonica;

        public Vladar()
        {

        }

        public Vladar(string ime, string prezime, DateTime datumRodjenja, DateTime datumSmrti, string mesto, string slika, string dinastija, string titula, string drzava, string prestonica)
        {
            Ime = ime;
            Prezime = prezime;
            DatumRodjenja = datumRodjenja;
            DatumSmrti = datumSmrti;
            Mesto = mesto;
            Slika = slika;
            Dinastija = dinastija;
            Titula = titula;
            Drzava = drzava;
            Prestonica = prestonica;

        }

        public void Copy(Vladar v)
        {
            Ime = v.Ime;
            Prezime = v.Prezime;
            DatumRodjenja = v.DatumRodjenja;
            DatumSmrti = v.DatumSmrti;
            Mesto = v.Mesto;
            Slika = v.Slika;
            Dinastija = v.Dinastija;
            Titula = v.Titula;
            Drzava = v.Drzava;
            Prestonica = v.Prestonica;
        }

        public override string ToString()
        {
            return "Vladar :" + Ime + " " + Prezime + " (" + DatumRodjenja.ToString("dd/MM/yyyy") + " - " + DatumSmrti.ToString("dd/MM/yyyy") + ")";
        }


        public string Dinastija
        {
            get
            {
                return dinastija;
            }
            set
            {
                if (value != dinastija)
                {
                    dinastija = value;
                    OnPropertyChanged("Dinastija");
                }
            }
        }

        public string Titula
        {
            get
            {
                return titula;
            }
            set
            {
                if (value != titula)
                {
                    titula = value;
                    OnPropertyChanged("Titula");
                }
            }
        }

        public string Drzava
        {
            get
            {
                return drzava;
            }
            set
            {
                if (value != drzava)
                {
                    drzava = value;
                    OnPropertyChanged("Drzava");
                }
            }
        }

        public string Prestonica
        {
            get
            {
                return prestonica;
            }
            set
            {
                if (value != prestonica)
                {
                    prestonica = value;
                    OnPropertyChanged("Prestonica");
                }
            }
        }
        
    }
}
