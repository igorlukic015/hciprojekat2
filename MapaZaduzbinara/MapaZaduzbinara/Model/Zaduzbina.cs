﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapaZaduzbinara.Model 
{
    public class Zaduzbina : INotifyPropertyChanged
    {
        private string naziv;
        private string mesto;
        private int godinaIzgradnje;
        private string opis;
        private Osoba zaduzbinar;
        private VrstaZaduzbine vrsta;
        private string ikonica;

        public override string ToString()
        {
            return vrsta.ToString() + " Naziv: " + naziv + " Mesto: " + mesto + " godinaIzgradnje: " + godinaIzgradnje +" Opis: " + opis + " Zaduzbinar: " + zaduzbinar.ToString() + " Ikonica: " + ikonica;
        }

        public string Naziv
        {
            get
            {
                return naziv;
            }
            set
            {
                if (value != naziv)
                {
                    naziv = value;
                    OnPropertyChanged("Naziv");
                }
            }
        }

        public string Mesto
        {
            get
            {
                return mesto;
            }
            set
            {
                if (value != mesto)
                {
                    mesto = value;
                    OnPropertyChanged("Mesto");
                }
            }
        }

        public int GodinaIzgradnje
        {
            get
            {
                return godinaIzgradnje;
            }
            set
            {
                if (value != godinaIzgradnje)
                {
                    godinaIzgradnje = value;
                    OnPropertyChanged("GodinaIzgradnje");
                }
            }
        }

        public string Opis
        {
            get
            {
                return opis;
            }
            set
            {
                if (value != opis)
                {
                    opis = value;
                    OnPropertyChanged("Opis");
                }
            }
        }

        public Osoba Zaduzbinar
        {
            get
            {
                return zaduzbinar;
            }
            set
            {
                if (value != zaduzbinar)
                {
                    zaduzbinar = value;
                    OnPropertyChanged("Zaduzbinar");
                }
            }
        }

        public VrstaZaduzbine Vrsta
        {
            get
            {
                return vrsta;
            }
            set
            {
                if (value != vrsta)
                {
                    vrsta = value;
                    OnPropertyChanged("Vrsta");
                }
            }
        }

        public string Ikonica
        {
            get
            {
                return ikonica;
            }
            set
            {
                if (value != ikonica)
                {
                    ikonica = value;
                    OnPropertyChanged("Ikonica");
                }
            }
        }

        public Zaduzbina()
        {

        }

        public Zaduzbina(string naziv, string mesto, int godinaIzgradnje, string opis, Osoba zaduzbinar, VrstaZaduzbine vrsta, string ikonica)
        {
            Naziv = naziv;
            Mesto = mesto;
            GodinaIzgradnje = godinaIzgradnje;
            Opis = opis;
            Zaduzbinar = zaduzbinar;
            Vrsta = vrsta;
            Ikonica = ikonica;
        }
        
        public void Copy(Zaduzbina z)
        {
            Naziv = z.naziv;
            Mesto = z.mesto;
            GodinaIzgradnje = z.godinaIzgradnje;
            Opis = z.opis;
            Zaduzbinar = z.zaduzbinar;
            Vrsta = z.vrsta;
            Ikonica = z.ikonica;
        }

        public event PropertyChangedEventHandler PropertyChanged;

        public virtual void OnPropertyChanged(string v)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(v));
            }
        }
    }
}
