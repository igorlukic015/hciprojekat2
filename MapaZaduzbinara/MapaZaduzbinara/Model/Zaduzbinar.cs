﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MapaZaduzbinara.Model
{
    public class Zaduzbinar : Osoba, INotifyPropertyChanged
    {
        private string zanimanje;

        public string Zanimanje
        {
            get
            {
                return zanimanje;
            }
            set
            {
                if (value != zanimanje)
                {
                    zanimanje = value;
                    OnPropertyChanged("Zanimanje");
                }
            }
        }

        public Zaduzbinar()
        {

        }

        public Zaduzbinar(string ime, string prezime, DateTime datumRodjenja, DateTime datumSmrti, string mesto, string slika,
            string zanimanje)
        {
            Ime = ime;
            Prezime = prezime;
            DatumRodjenja = datumRodjenja;
            DatumSmrti = datumSmrti;
            Mesto = mesto;
            Zanimanje = zanimanje;
            Slika = slika;

        }

        public override string ToString()
        {
            return "Zaduzbinar :" + Ime + " " + Prezime + " (" + DatumRodjenja.ToString("dd/MM/yyyy") + " - " + DatumSmrti.ToString("dd/MM/yyyy") + ")";
        }

        public void Copy(Zaduzbinar z)
        {
            Ime = z.Ime;
            Prezime = z.Prezime;
            DatumRodjenja = z.DatumRodjenja;
            DatumSmrti = z.DatumSmrti;
            Mesto = z.Mesto;
            Zanimanje = z.zanimanje;
            Slika = z.Slika;
        }

       
    }
}
