﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MapaZaduzbinara.Model
{
    public class BazaPodataka
    {
        private ObservableCollection<Zaduzbinar> zaduzbinari = new ObservableCollection<Zaduzbinar>();
        private ObservableCollection<Vladar> vladari = new ObservableCollection<Vladar>();
        private ObservableCollection<Zaduzbina> zaduzbine = new ObservableCollection<Zaduzbina>();

        private string putanjaZaduzbinari = null;
        private string putanjaVladari = null;
        private string putanjaZaduzbine = null;

        public BazaPodataka()
        {
            putanjaZaduzbinari = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "zaduzbinari.txt");
            putanjaVladari = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "vladari.txt");
            putanjaZaduzbine = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "zaduzbine.txt");
        }

        public void ucitajZaduzbinare()
        {
            if (File.Exists(putanjaZaduzbinari))
            {

                using (StreamReader reader = File.OpenText(putanjaZaduzbinari))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    zaduzbinari = (ObservableCollection<Zaduzbinar>)serializer.Deserialize(reader, typeof(ObservableCollection<Zaduzbinar>));
                }
            }
            else
            {
                zaduzbinari = new ObservableCollection<Zaduzbinar>();
            }


        }

        public void ucitajZaduzbine()
        {
            if (File.Exists(putanjaZaduzbine))
            {

                using (StreamReader reader = File.OpenText(putanjaZaduzbine))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    zaduzbine = (ObservableCollection<Zaduzbina>)serializer.Deserialize(reader, typeof(ObservableCollection<Zaduzbina>));
                }
            }
            else
            {
                zaduzbine = new ObservableCollection<Zaduzbina>();
            }


        }

        public void ucitajVladare()
        {
            if (File.Exists(putanjaVladari))
            {

                using (StreamReader reader = File.OpenText(putanjaVladari))
                {
                    JsonSerializer serializer = new JsonSerializer();
                    vladari = (ObservableCollection<Vladar>)serializer.Deserialize(reader, typeof(ObservableCollection<Vladar>));
                }
            }
            else
            {
                vladari = new ObservableCollection<Vladar>();
            }


        }

        public void save()
        {

            using (StreamWriter writer = File.CreateText(putanjaVladari))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, vladari);
                writer.Close();
            }

            using (StreamWriter writer = File.CreateText(putanjaZaduzbinari))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, zaduzbinari);
                writer.Close();
            }


            using (StreamWriter writer = File.CreateText(putanjaZaduzbine))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, zaduzbine);
                writer.Close();
            }


        }

        public void sacuvajVladare()
        {
            using (StreamWriter writer = File.CreateText(putanjaVladari))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, vladari);
                writer.Close();
            }


        }

        public void sacuvajZaduzbinare()
        {
            using (StreamWriter writer = File.CreateText(putanjaZaduzbinari))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, zaduzbinari);
                writer.Close();
            }


        }

        public void sacuvajZaduzbine()
        {
            using (StreamWriter writer = File.CreateText(putanjaZaduzbine))
            {
                JsonSerializer serializer = new JsonSerializer();
                serializer.Serialize(writer, zaduzbine);
                writer.Close();
            }


        }

        public bool noviVladar(Vladar v)
        {
            foreach (Vladar vladar in vladari)
            {
                if (vladar.Ime == v.Ime && vladar.Prezime == v.Prezime && vladar.DatumRodjenja == v.DatumRodjenja)
                { 
                    return false;
                }
            }
            vladari.Add(v);
            sacuvajVladare();

            return true;
        }

        public bool noviZaduzbinar(Zaduzbinar z)
        {
            foreach (Zaduzbinar zaduzbinar in zaduzbinari)
            {
                if (zaduzbinar.Ime == z.Ime && zaduzbinar.Prezime == z.Prezime && zaduzbinar.DatumRodjenja == z.DatumRodjenja)
                {
                    return false;
                }
            }
            zaduzbinari.Add(z);
            sacuvajZaduzbinare();

            return true;
        }

        public bool novaZaduzbina(Zaduzbina z)
        {
            foreach (Zaduzbina zaduzbina in zaduzbine)
            {
                if (zaduzbina.Naziv == z.Naziv && zaduzbina.GodinaIzgradnje == z.GodinaIzgradnje)
                {
                    return false;
                }
            }
            zaduzbine.Add(z);
            sacuvajZaduzbine();

            return true;
        }

        public bool brisanjeVladara(Vladar v)
        {

            foreach (Vladar vladar in vladari)
            {
                if (vladar.Ime == v.Ime && vladar.Prezime == v.Prezime && vladar.DatumRodjenja == v.DatumRodjenja)
                {
                    vladari.Remove(v);
                    sacuvajVladare();

                    return true;
                }
            }

            return false;
        }

        public bool brisanjeZaduzbinara(Zaduzbinar z)
        {

            foreach (Zaduzbinar zaduzbinar in zaduzbinari)
            {
                if (zaduzbinar.Ime == z.Ime && zaduzbinar.Prezime == z.Prezime && zaduzbinar.DatumRodjenja == z.DatumRodjenja)
                {
                    zaduzbinari.Remove(z);
                    sacuvajZaduzbinare();

                    return true;
                }
            }

            return false;
        }

        public bool brisanjeZaduzbina(Zaduzbina z)
        {

            foreach (Zaduzbina zaduzbina in zaduzbine)
            {
                if (zaduzbina.Naziv == z.Naziv && zaduzbina.GodinaIzgradnje == z.GodinaIzgradnje)
                {
                    zaduzbine.Remove(z);
                    sacuvajZaduzbine();

                    return true;
                }
            }

            return false;
        }

        public ObservableCollection<Vladar> Vladari
        {
            get { return vladari; }
            set { vladari = value; }
        }

        public ObservableCollection<Zaduzbinar> Zaduzbinari
        {
            get { return zaduzbinari; }
            set { zaduzbinari = value; }
        }

        public ObservableCollection<Zaduzbina> Zaduzbine
        {
            get { return zaduzbine; }
            set { zaduzbine = value; }
        }

    }
}
