﻿using MapaZaduzbinara.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MapaZaduzbinara.Dialog
{
    /// <summary>
    /// Interaction logic for IzmenaZaduzbina.xaml
    /// </summary>
    public partial class IzmenaZaduzbina : Window
    {
        private string slika = null;
        private Zaduzbina zaduzbina = null;
        private BazaPodataka baza = null;
        private ObservableCollection<Osoba> osobe = null;
        private VrstaZaduzbine vrstaZaduzbine;

        public IzmenaZaduzbina(Zaduzbina z, BazaPodataka b, ObservableCollection<Osoba> o)
        {
            zaduzbina = z;
            baza = b;
            osobe = o;
            InitializeComponent();
            naziv_textBoxZaduzbina.Text = z.Naziv;
            mesto_textBoxZaduzbina.Text = z.Mesto;
            godinaIzgradnje_textBoxZaduzbina.Text = z.GodinaIzgradnje.ToString();
            textBoxOpisZaduzbina.Text = z.Opis;
            zaduzbinar_comboBoxZaduzbina.ItemsSource = osobe;
            slikaZaduzbina.Source = new BitmapImage(new Uri( z.Ikonica));


        }
        private void VerskiObjekat_Checked(object sender, RoutedEventArgs e)
        {
            vrstaZaduzbine = VrstaZaduzbine.VERSKI_OBJEKAT;
        }

        private void OtkaziIzmene_Click(object sender, RoutedEventArgs e)
        {

        }

        private void izaberiSlikuZaduzbina_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Izaberi ikonicu";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                slikaZaduzbina.Source = new BitmapImage(new Uri(fileDialog.FileName));
                slika = fileDialog.FileName;
            }
        }

        private void UstanovaKulture_Checked(object sender, RoutedEventArgs e)
        {
            vrstaZaduzbine = VrstaZaduzbine.USTANOVA_KULTURE;
        }

        private void SacuvajIzmene_Click(object sender, RoutedEventArgs e)
        {
            string naziv = naziv_textBoxZaduzbina.Text;
            string mesto = mesto_textBoxZaduzbina.Text;
            string godinaIzgradnje = godinaIzgradnje_textBoxZaduzbina.Text;
            string zaduzbinar = zaduzbinar_comboBoxZaduzbina.Text;
            string opis = textBoxOpisZaduzbina.Text;

            if (naziv.Equals("") || mesto.Equals("") || godinaIzgradnje.Equals("") || zaduzbinar.Equals(""))
            {
                System.Windows.MessageBox.Show("Niste popunili sva tražena polja za unos zadužbine");
                return;
            }

            if (!Regex.IsMatch(naziv, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Naziv zaduzbine ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(naziv, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Mesto zaduzbine ne sme da sadrzi brojeve");
                return;
            }



            int godina = -1;
            try
            {
                godina = Int32.Parse(godinaIzgradnje);
                Console.WriteLine(godina);
            }
            catch (FormatException)
            {
                Console.WriteLine($"Godina izgradnje mora biti ceo broj '{godinaIzgradnje}'");
            }

            if (slika == null)
            {
                string privremeni = slikaZaduzbina.Source.ToString();
                string[] privTokens = privremeni.Split(',');
                slika = privTokens[3];

            }

            Osoba z = (Osoba)zaduzbinar_comboBoxZaduzbina.SelectedValue;
            Zaduzbina zaduzbina = new Zaduzbina(naziv, mesto, godina, opis, z, vrstaZaduzbine, slika);

            slika = null;
            slikaZaduzbina.Source = new BitmapImage(new Uri("pack://application:,,,/MapaZaduzbinara;component/Icons/beer.png"));
            //System.Windows.MessageBox.Show(zaduzbina.ToString());
            bool passed = baza.novaZaduzbina(zaduzbina);
            if (passed)
            {
                System.Windows.MessageBox.Show("Uspešno dodata nova zadužbina.", "Dodavanje zadužbina");
                baza.sacuvajZaduzbine();

            }
            else
                System.Windows.MessageBox.Show("Vec postoji uneta zadužbina u bazi podataka!", "Dodavanje zadužbina");
        }

    }
}
