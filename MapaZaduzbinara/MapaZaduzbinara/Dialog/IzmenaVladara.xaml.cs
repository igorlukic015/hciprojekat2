﻿using MapaZaduzbinara.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MapaZaduzbinara.Dialog
{
    /// <summary>
    /// Interaction logic for IzmenaVladara.xaml
    /// </summary>
    public partial class IzmenaVladara : Window
    {
        private string slika = null;
        private Vladar vladar = null;
        private BazaPodataka baza = null;
        
        public IzmenaVladara(Vladar v, BazaPodataka b)
        {
            InitializeComponent();
            vladar = v;
            ime_textBoxVladar.Text = v.Ime;
            prezime_textBoxVladar.Text = v.Prezime;
            datumPickerRodjenjeVladar.SelectedDate = v.DatumRodjenja;
            datumPickerSmrtiVladar.SelectedDate = v.DatumSmrti;
            textBoxMestoRodjenjaVladar.Text = v.Mesto;
            dinastija_textBoxVladar.Text = v.Dinastija;
            titula_textBoxVladar.Text = v.Titula;
            drzava_textBoxVladar.Text = v.Drzava;
            prestonica_textBoxVladar.Text = v.Prestonica;
            //slikaVladar.Source = new BitmapImage(new Uri("pack://application:,,,/MapaZaduzbinara;component/Icons/beer.png"));
            slikaVladar.Source = new BitmapImage(new Uri(v.Slika));
            baza = b;

        }

        private void izaberiSlikuVladar_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Izaberi ikonicu";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                slikaVladar.Source = new BitmapImage(new Uri(fileDialog.FileName));
                slika = fileDialog.FileName;
            }
        }
        private void IzmeniVladara_Click(object sender, RoutedEventArgs e)
        {
            string ime = ime_textBoxVladar.Text;
            string prezime = prezime_textBoxVladar.Text;
            if (datumPickerRodjenjeVladar.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum rodjenja vladara");
                return;
            }

            if (datumPickerSmrtiVladar.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum smrti vladara");
                return;
            }

            DateTime datumRodjenja = (DateTime)datumPickerRodjenjeVladar.SelectedDate;
            DateTime datumSmrti = (DateTime)datumPickerSmrtiVladar.SelectedDate;
            string mesto = textBoxMestoRodjenjaVladar.Text;
            string dinastija = dinastija_textBoxVladar.Text;
            string titula = titula_textBoxVladar.Text;
            string drzava = drzava_textBoxVladar.Text;
            string prestonica = prestonica_textBoxVladar.Text;

            if (ime.Equals("") || prezime.Equals("") || datumRodjenja.Equals("") || datumSmrti.Equals("") || mesto.Equals("") || dinastija.Equals("") || titula.Equals("") || drzava.Equals("") || prestonica.Equals(""))
            {
                System.Windows.MessageBox.Show("Niste popunili sva polja za unos vladara");
                return;
            }

            if (!Regex.IsMatch(ime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Ime vladara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(prezime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Prezime vladara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(mesto, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Mesto rodjenja vladara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(titula, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Titula vladara ne sme da sadrzi brojeve");
                return;
            }
            if (!Regex.IsMatch(dinastija, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Dinastija vladara ne sme da sadrzi brojeve");
                return;
            }
            if (!Regex.IsMatch(drzava, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Drzava vladara ne sme da sadrzi brojeve");
                return;
            }
            if (!Regex.IsMatch(prestonica, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Prestonica vladara ne sme da sadrzi brojeve");
                return;
            }

            if (slika == null)
            {
                string privremeni = slikaVladar.Source.ToString();
                string[] privTokens = privremeni.Split(',');
                slika = privTokens[3];

            }

            Vladar vladarNovi = new Vladar(ime, prezime, datumRodjenja, datumSmrti, mesto, slika, dinastija, titula, drzava, prestonica);
            bool bris = baza.brisanjeVladara(vladar);
            if (bris) { 
                bool passed = baza.noviVladar(vladarNovi);
                if (passed)
                {
                    System.Windows.MessageBox.Show("Uspešno dodat novi vladar.", "Dodavanje vladara");
                    baza.sacuvajVladare();
                }
                else
                    System.Windows.MessageBox.Show("Vec postoji uneti vladar u bazi podataka!", "Dodavanje vladara");
            }
            else
                System.Windows.MessageBox.Show("Ne postoji uneti vladar u bazi podataka!", "Dodavanje vladara");
        }
    }


}
