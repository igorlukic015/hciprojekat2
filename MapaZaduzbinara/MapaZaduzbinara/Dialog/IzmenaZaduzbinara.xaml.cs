﻿using MapaZaduzbinara.Model;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MapaZaduzbinara.Dialog
{
    /// <summary>
    /// Interaction logic for IzmeniZaduzbinara.xaml
    /// </summary>
    public partial class IzmenaZaduzbinara : Window
    {
        private string slika = null;
        private Zaduzbinar zaduzbinar = null;
        private BazaPodataka baza = null;


        public IzmenaZaduzbinara(Zaduzbinar z, BazaPodataka b)
        {
            InitializeComponent();
            zaduzbinar = z;
            ime_textBox.Text = z.Ime;
            zanimanje_textBox.Text = z.Zanimanje;
            prezime_textBox.Text = z.Prezime;
            datumPickerR.SelectedDate = z.DatumRodjenja;
            datumPickerS.SelectedDate = z.DatumSmrti;
            mesto_textBox.Text = z.Mesto;


            //slikaZaduzbinar.Source = new BitmapImage(new Uri("pack://application:,,,/MapaZaduzbinara;component/Icons/beer.png"));
            slikaZaduzbinar.Source = new BitmapImage(new Uri(z.Slika));

            baza = b;

        }

        private void izmeniIkonicu_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();


            fileDialog.Title = "Izaberi ikonicu";
            fileDialog.Filter = "Images|*.jpg;*.jpeg;*.png|" +
                                "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" +
                                "Portable Network Graphic (*.png)|*.png";
            if (fileDialog.ShowDialog() == true)
            {
                slikaZaduzbinar.Source = new BitmapImage(new Uri(fileDialog.FileName));
                slika = fileDialog.FileName;
            }
        }

        private void odustani_Click(object sender, RoutedEventArgs e)
        {

        }

        private void sacuvaj_Click(object sender, RoutedEventArgs e)
        {
            string ime = ime_textBox.Text;
            string prezime = prezime_textBox.Text;
            string zanimanje = zanimanje_textBox.Text;
            if (datumPickerR.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum rodjenja zadužbinara");
                return;
            }

            if (datumPickerS.SelectedDate == null)
            {
                System.Windows.MessageBox.Show("Niste uneli datum smrti zadužbinara");
                return;
            }

            DateTime datumRodjenja = (DateTime)datumPickerR.SelectedDate;
            DateTime datumSmrti = (DateTime)datumPickerS.SelectedDate;
            string mesto = mesto_textBox.Text;




            if (ime.Equals("") || prezime.Equals("") || datumRodjenja.Equals("") || datumSmrti.Equals("") || mesto.Equals(""))
            {
                System.Windows.MessageBox.Show("Niste popunili sva polja za unos zadužbinara");
                return;
            }

            if (!Regex.IsMatch(ime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Ime zadužbinara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(prezime, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Prezime zadužbinara ne sme da sadrzi brojeve");
                return;
            }

            if (!Regex.IsMatch(mesto, @"^[a-zA-Z]+$"))
            {
                System.Windows.MessageBox.Show("Mesto rodjenja zadužbinara ne sme da sadrzi brojeve");
                return;
            }

            if (slika == null)
            {
                string privremeni = slikaZaduzbinar.Source.ToString();
                string[] privTokens = privremeni.Split(',');
                slika = privTokens[3];

            }


            Zaduzbinar zaduzbinarNovi = new Zaduzbinar(ime, prezime, datumRodjenja, datumSmrti, mesto, zanimanje, slika);
            bool bris = baza.brisanjeZaduzbinara(zaduzbinar);
            if (bris)
            {
                bool passed = baza.noviZaduzbinar(zaduzbinarNovi);
                if (passed)
                {
                    System.Windows.MessageBox.Show("Uspešno dodat novi zadužbinar.", "Dodavanje zadužbinara");
                    baza.sacuvajZaduzbinare();
                }
                else
                    System.Windows.MessageBox.Show("Već postoji uneti zadužbinar u bazi podataka!", "Dodavanje zadužbinara");
            }
            else
                System.Windows.MessageBox.Show("Ne postoji uneti zadužbinar u bazi podataka!", "Dodavanje zadužbinara");
        }
    }



}

           

          













